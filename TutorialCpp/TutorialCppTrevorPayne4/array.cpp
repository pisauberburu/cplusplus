/*
 Tutorial C++ Trevor Payne no. 4 : Array, Chars, & Strings

*/

#include <iostream>
#include <string>

using namespace std;

int main1(){

    int my_array[2];
    my_array[0] = -5;
    my_array[1] = 1 ;


    /*
    Rememmber : Human count from 1 --> 1,2,3 , but
    computer count from 0 --> 0,1,2,...
    */

    int my_array2[3] = {0,1,2} ; // only 3 elements [0],[1],[2],
                                 //there is no [3] , (4th element)
    int my_array3[3] = { 4 }; // 4 will be the 1st element --> my_array[0]
    int my_array4[]{7,7,7} ;
    int my_array5[4]{} ;

    //int my_array0[] ;

    //cout<< "my_array0[]" << my_array0<<endl;

    cout<< "my_array =" << my_array[0] <<endl;
    cout<<" my_array2[2]=" << my_array2[2]<<endl;
    cout<<" my_array3[0] =" << my_array3[0]<<endl;
    cout<<" my_array5[0] =" << my_array5[0] <<endl;


    cout<<" my_array4 =" << my_array4[1] <<endl;
    // my_array4[2]= {7,7,7} ; // cannot change size array , YOU USE VECTOR
     // cout<<" my_array4 =" << my_array4[1] <<endl;



    string z ;
    getline(cin, z);
    return 0;
}
