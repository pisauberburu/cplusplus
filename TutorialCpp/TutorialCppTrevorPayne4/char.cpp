/*
 Tutorial C++ Trevor Payne no. 4 : Array, Chars, & Strings

*/

#include <iostream>
#include <string>

using namespace std;

int main(){
    char a[] = "asdf" ;
    char b[5] = "asdf" ;
    char b2[2] = "a" ;
    char c[]{'a' ,'b' , '\0' } ;
    char d[]{'a' ,'b' , '\0','c','\0' } ; // \0 for NULL (stop input character)

    cout<<" a="<<a <<endl;
    cout<<" b="<<b <<endl;
      cout<<" b[0]="<<b[0] <<endl;
     cout<<" b[1]="<<b[1] <<endl;
     cout<<" b[2]="<<b[2] <<endl;
      cout<<" b[3]="<<b[3] <<endl;
       cout<<" b[4]="<<b[4] <<endl;
        cout<<" b[5]="<<b[5] <<endl;
     cout<<" b2="<<b2 <<endl;
    cout<<" c="<<c <<endl;
       cout<<" d="<<d <<endl;





return 0;
}

