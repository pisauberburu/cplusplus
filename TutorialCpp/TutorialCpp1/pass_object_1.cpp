#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;
protected:

public:
    //Complex(): real(8), imag(8) {}
    Complex(){
        real = 9 ;
        imag = 9 ;
    }
    void readData(){
        cout<<"Enter the value of real number and imaginary number:"<<endl;
        cout<<"Real number:";
        cin>>real;
        cout<<" + imaginary number:" ;
        cin>>imag ;

    }

    void addComplexNumber(Complex c1 , Complex c2){
        real = c1.real + c2.real ;
        imag = c1.imag + c2.imag ;

    }

    void displayResult()
    {
        cout<< "The sum value=" <<real<<" + (i)"<<imag<<endl;
    }

    void doubleComplex(Complex c1) ;

};


void Complex::doubleComplex(Complex d){
    d.real = d.real*2 ;
    d.imag = d.imag*2 ;
    cout<<"After double \n Real="<<d.real<< " +(i)"<<d.imag<<endl;
}


int main2 (){
    Complex c1, c2, c3 ;

    c1.displayResult() ;
    c1.readData();
    c2.readData();

    c3.addComplexNumber(c1,c2) ;
    c3.displayResult();

     c1.doubleComplex(c1) ;

    return 0;
}
