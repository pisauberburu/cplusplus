#include <iostream>
using namespace std;

class Person{
private:
    int age;
    int gender ;
public:
    int getAge(){ return age ;}

    void setAge(int &in_age){
        age = in_age * 2 ;
    }

};

int maine(){
    // Ada 3 cara mengakses Clas
    int input1 = 17 ;
    //(1)
    Person *p = new Person;
    cout<<"The Age: "<<(*p).getAge()<<endl;
    (*p).setAge(input1)  ;

    cout<<"Input: "<<input1<<endl;

    cout<<"The Age: "<<(*p).getAge()<<endl;

    // (2)
    //p->age = 15 ;
    cout<<"The Age: "<<p->getAge()<<endl;

    delete p;



return 0;

}
