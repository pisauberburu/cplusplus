#include <iostream>


using namespace std;


class Entity{
    int Y , X ;


public:
    int Print() const
    {

    }

};


int main1(){

const int MAX_AGE = 90 ;
cout<<&MAX_AGE<<endl;


int *a = new int ;
*a = 2 ;

a = (int*) &MAX_AGE ;

cout<<a<<endl;
cout<<*a<<endl;
cout<<"-----------------------" <<endl;

const int *b = new int ; //you cannot change the content of the pointer
 // *b= 10 ; //Cannot
 b = (int*) &MAX_AGE ;

 cout<<b<<endl;
cout<<*b<<endl;

cout<<"-----------------------" <<endl;
int * const c = new int ; //you cannot re-assignment reference
 // *b= 10 ; //Cannot
 //c = (int*) &MAX_AGE ;
 *c = 5;

 cout<<c<<endl;
cout<<*c<<endl;

cout<<"-----------------------" <<endl;
const int * const d = new int(23) ; //you cannot do both
 // *d= 10 ; //Cannot
 //d = (int*) &MAX_AGE ;
 //  d = nullptr ;

 cout<<d<<endl;
cout<<*d<<endl;


return 0 ;
}
