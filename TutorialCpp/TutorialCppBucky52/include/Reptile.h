#ifndef REPTILE_H
#define REPTILE_H


class Reptile : public Animal
{
    public:
        Reptile();
        void nameSpecies() ;
        void includedClass() ;
    protected:
    private:
};

#endif // REPTILE_H
