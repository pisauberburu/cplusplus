// const function is complicated
/*
    Video from Youtube Jamie King ,
    C++ const functions (const correctness)
*/

#include <iostream>
#include <string>

using namespace std;

#define TRIAL(x) cout<<" \n Lets try again.. Trial-"<<x<<endl

const int MY_INT = 5 ;

struct Grass{
    float   weight ;
    char type = 'N' ;
    // char * type = "N" ; //what's difference "" and '' in char ?

    /* Check this :
    https://www.geeksforgeeks.org/whats-difference-between-char-s-and-char-s-in-c/
    */


    string type2 = "B" ; // cannot use char , I don't know why ....
    void SetGrassWeight(float weight_in ){
        weight = weight_in ;

    } ;
    void SetType(char *typein){
        type = *typein ;
    } ;
    void SetType2(string typein){
        type2 = typein ;
    } ;

};

class Cow{
    int mooCount ;
    float grass ;
public:
    Cow() { mooCount =0 ;

    }

    void addMoo() const { cout<<mooCount<<endl ; }
    void sayMoo() const { cout<<"Mooooo! "<<endl;  } ;
    void milkProduct(const double * litre) ;
    void eatGrass( const Grass *Gr, int * level)  ;

};

void Cow::milkProduct(const double * litre){
    double totalmilk = (*litre) * 10 ;
    cout<<"The cow products: "<<totalmilk<<" liters of milk"<<endl ;
}

void Cow::eatGrass( const Grass *Gr, int *level){
          float total =   2 * (Gr->weight);
        cout<<"The cow eats : tipe "<<Gr->type<<" grass, and "<< total << " kg grass "<<endl ;
        cout<<"Level:"<<(*level)<<endl;

    } ;



int  main(){
    // MY_INT = 2 ;  // You cant do this!!
    cout<<"MY_INT = "<<MY_INT<<endl ;

    char t = 'X' ;
     // Alt 1
    //float weight = 12.35 ;
    //float * grass_arg = &weight ;

    // Alt 2:
     float grass_arg = 16.35 ;


    //Alt 3 :
   // float weight = 21.55 ;
    //float &grass_arg = weight ;


    //Alt 1A

    char in = 'Z' ;
    char &in_typeC = in ;
    string in_type2 = "Low" ;
    int level = 4 ;



    // g->type = "C";
     // g->weight = 14.31 ;


    double litre = 12.501;

     TRIAL(1) ;
     Grass  g ;
     g.SetGrassWeight(grass_arg) ;
     g.SetType(&in_typeC) ;
     g.SetType2(in_type2) ;

     Grass &d = g ;
     d.type = g.type ;
     d.weight = g.weight;

    Cow normal_obj ;
    normal_obj.addMoo() ;
    normal_obj.sayMoo() ;
    normal_obj.milkProduct(&litre) ;
    normal_obj.eatGrass(&g,&level) ;

    // Trial 2
      TRIAL(2) ;
     Grass g2 ;
     g2.weight = 76.34 ;

     Cow normal_obj2 ;
     normal_obj2.addMoo() ;
    normal_obj2.sayMoo() ;
    normal_obj2.milkProduct(&litre) ;
    normal_obj2.eatGrass(&g2,&level) ;


    // Trial 3
    TRIAL(3) ;
    Grass *g3 = new Grass;
    g3->type = 'M' ;
    g3->weight = 5.12 ;
    Cow normal_obj3 ;
     normal_obj3.addMoo() ;
    normal_obj3.sayMoo() ;
    normal_obj3.milkProduct(&litre) ;
    normal_obj3.eatGrass(g3,&level) ;


    //Trial 4
    /*   //CAUSED CORE DUMP, Why?
    Grass *g4 ;
     char in2 = 'Q' ;
    char &in_typeC2 = in2 ;
    g4->SetType(&in_typeC2) ;
    Cow normal_obj4 ;
     normal_obj4.addMoo() ;
    normal_obj4.sayMoo() ;
    normal_obj4.milkProduct(&litre) ;
    normal_obj4.eatGrass(g4,&level) ;

    */


    const Cow const_obj ;
    const_obj.addMoo() ;
    const_obj.sayMoo() ;


 return 0 ;
}
