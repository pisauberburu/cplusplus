/*
    From Youtube "thenewboston"
        Buckys C++ Programming Tutorial - 39
*/

#include <iostream>

using namespace std ;

int main(){
    int fish = 5 ;
    cout << "fish =" << fish << ", stored in :"<< &fish << endl;

    int &koi = fish ;
     cout << "koi =" << koi << ", stored in :"<< &koi << endl;

     int change ;
     change = 8 ;
     cout <<"Koi change by "<<change << endl;
     koi = koi + change ;

     cout << "\n  At the End: fish =" << fish << ", stored in :"<< &fish << endl;
     cout << "\n  At the End: koi =" << koi << ", stored in :"<< &koi << endl;

      change = -1 ;
     cout <<"Fish change by "<<change << endl;
     fish = fish + change ;

     cout << "\n  At the End: fish =" << fish << ", stored in :"<< &fish << endl;
     cout << "\n  At the End: koi =" << koi << ", stored in :"<< &koi << endl;

}
