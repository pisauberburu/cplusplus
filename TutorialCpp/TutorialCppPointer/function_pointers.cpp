/*
This tutorial is covering function pointers : assign a function to variable
from  youtube video : The Cherno Project : Function Pointers C++


*/

#include <iostream>
#include <string>

using namespace std ;

void Helloworld(int a){
    cout<<"Hello World :"<< a <<endl ;

}

int main2(){
    typedef void (*HelloWorldFunction)(int) ;

    HelloWorldFunction thefunction = Helloworld ;

    thefunction(7) ;

     return 0 ;
}
