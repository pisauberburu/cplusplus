/*
  Video from Youtube: xoaxdotnet 'C++ Beginner Tutorial'
  C++ Console Lesson 17: References and Pointers
  https://www.youtube.com/watch?v=LwAPKuJpvmA&list=PLA68C1F33757B4A38&index=17


*/


#include <iostream>
#include <string>

using namespace std ;

int main(){

/* References:
 1. MUST BE INTITIALIZED AT DECLARATION
 2. CANNOT BE RE-ASSIGNED
 */

int iInt = 10 ;
int &irRefToInt = iInt ;

cout<<" iInt ="<< iInt<<endl;
cout<<" irRefToInt ="<< irRefToInt<<endl;


// POINTER =======================

  int *ipPtrToInt = 0 ;
 int PtrInt = 10 ;
 int AnotherPtrInt = 20 ;

ipPtrToInt = &PtrInt ;



cout << "*ipPtrToInt = "<<*ipPtrToInt<<endl ;




ipPtrToInt = &AnotherPtrInt ;
 AnotherPtrInt = 3 ;
cout << "*ipPtrToInt  = "<<*ipPtrToInt<<endl ;
cout << "AnotherPtrInt  = "<<AnotherPtrInt<<endl ;

 *ipPtrToInt = PtrInt ;
cout << "*AnotherPtrInt  = "<<AnotherPtrInt<<endl ;


;
 ipPtrToInt = &PtrInt ;
 *ipPtrToInt = 99 ;
cout << "*ipPtrToInt = "<<*ipPtrToInt<<endl ;

cout << "*AnotherPtrInt  = "<<AnotherPtrInt<<endl ;


};
