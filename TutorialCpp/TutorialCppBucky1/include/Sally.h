#ifndef SALLY_H
#define SALLY_H

namespace rian{
    void gogo();
}

class Sally
{
    public:
        Sally();
        void printCrap();
        void SallyFullName() const;
        virtual ~Sally();
    protected:
    private:
        rian::void gogo(){
            cout<<"This is Gogo"<<endl;
        }

};

#endif // SALLY_H
