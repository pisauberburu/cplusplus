#include <iostream>
#include <string>
#include "Sally.h"

using namespace std;


int main(){

    Sally sallyObject ;
    Sally *sallyPointer = &sallyObject ;

    sallyObject.printCrap() ;
    sallyPointer->printCrap() ;

    const Sally t ; // constan function only can be called from constant object
    t.SallyFullName();

return 0;
}
