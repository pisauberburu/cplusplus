#include <iostream>
#include <string>

/*
Learn Pointer in C++
https://www.youtube.com/watch?v=DvUYbUn9uQ4

*/

using namespace std;
string x;


int main() {
	int a = 5;
	int b = a;
	int &c = b;


	cout << "Test for Pointer & Reference" << endl;

	cout << "Result a =" << a << endl;
	cout << "Result b = " << b << endl;
	cout << "Result c = " << c << endl;
	cout << "Result &a = " << &a << endl;
	cout << "Result &b = " << &b << endl;
	cout << "Result &c = " << &c << endl;
	cout << "Result *&c = " << *&c << endl;


	// int *ptr = 888;
	int *ptr = &b;
	int *ptr2 = ptr;


	cout << "Result ptr:" << ptr << endl;
	cout << "Result *ptr:" << *ptr << endl;

	cout<<"\n Change b \n";
	*ptr = 121 ;
	cout << "Result b :" << b << endl;



	// Now we modified
	*ptr = 56;
	*&b = 127; // replacement in *&b also meaning replacement *ptr ad versa

	*&a = *&b * 10;

	//Switch
	*ptr2 = 1001;

	cout << "Result a (after modified) : " << a << endl;
	cout << "Result b (after modified) : " << b << endl;
	cout << "Result c (after modified) : " << c << endl;
	cout << "Result &a = " << &a << endl;
	cout << "Result *&b = " << *&b << endl;
	cout << "Result ptr (after modified):" << ptr << endl;
	cout << "Result *ptr (after modified):" << *ptr << endl;




	getline(cin, x);


	return 0;
}
